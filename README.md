# from-after

## API

### `operand::fromAfter(pattern)`

Returns the trimmed string.

Looks for a pattern in a string and, if the pattern is found, cuts off the string after the first occurrence of the pattern.

Designed for use with `::` function bind syntax, as the `this` property should be the string to trim.

#### operand

String to trim.

#### pattern

String to look for and trim after.

## Use Case

```js
import {fromAfter} from 'from-after'

const filepath = '~/app/node_modules/foo/bar.js'

filepath::fromAfter('node_modules/')
// -> 'foo/bar.js`
```

## See Also

- [from-before](https://www.npmjs.com/package/from-before) ⟼
- [from-after](https://www.npmjs.com/package/from-after) ⇤
- [until-before](https://www.npmjs.com/package/until-before) ⇥
- [until-after](https://www.npmjs.com/package/until-after) ⟻

```js
const text = 'goodbye cruel world'

text::fromBefore('cruel')  // 'cruel world'
text::fromAfter('cruel')   // ' world'
text::untilBefore('cruel') // 'goodbye '
text::untilAfter('cruel')  // 'goodbye cruel'
```

## Colophon

Made by Sebastiaan Deckers in Singapore 🇸🇬
