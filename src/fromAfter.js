export function fromAfter (pattern) {
  return this.includes(pattern)
    ? this.substring(this.indexOf(pattern) + pattern.length)
    : this
}
